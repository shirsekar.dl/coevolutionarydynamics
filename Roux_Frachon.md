# A GWAS in *A. thalina* to decipher the adaptive genetics of quantitative disease resistance

## 1. Summary
Adaptive value  of plant quantitative disease resistance (QDR) in ecological settings has remained relatively unexplored aspect of host-pathogen interactions. How a specific host interacts with pathogen/s within native biotic-abiotic constraints and whether that leaves eco-evolutionary signatures in the genomes of both host and pathogens are two important questions that need to be tackled in order to understand the QDR. 

Roux and Frachon have presented here results from an extension of a previously conducted excellent *Arabidopsis* field study (Frachon et al 2017). Using disease phenotyping while accounting for the covariates measured in Frachon et al 2017 and performing genome wide association analysis they show that the environmental heterogeneity at micro-habitat scale selects for a few quantitative trait loci (QTLs) that could be contributing towards disease resistance and fecundity.

My major concern about the work presented here are related to how the "disease" is defined throughout the work and how that definition might have implication on subsequent analysis. 

Authors cite Roux et al 2010 (Ref 33 in the manuscript) as their way of classifying the plants with specific  symptoms (chlorosis, water soaking and cell death) as diseased. Roux et al 2010 conducted these experiments with syringe inoculation described in Korves and Bergelson (2003) and scored the symptoms on a scale of 0 - 6 (no symptoms to 66% or more infected area).\
Where as Bartoli et al 2018 (Ref 26) classified the plants on binary scale (presence or absence of visible disease symptoms) current work uses the scale of 0 - 8. 
The disease scoring scheme presented in this work is highly subjective with no keys to scale described anywhere in the method (or results). Was there any particular reason to switch from the 0-6 scale to 0-8 scale? I could not find any justification of this anywhere although author rely on Roux et al 2010 symptom categories. I was left wondering whether specific value on the scale represents a particular combination of symptoms. Based on Fig1A, one can easily mis-categorize plants from intermediate categories (5-6, 6,7,8). The subjective disease scoring scheme chosen by the authors definitely has a substantial effect on the association analysis.\
Ideally, if the image data for each scored plant is available, the image segmentation followed by an objective criteria based on chlorotic/necrotic plant tissue will result in a more accurate quantitative description of the symptoms that will improve the GWAS analysis further.

The disease phenotyping is especially important while connecting effect of symptoms (disease severity) to the fecundity (fitness). Kover and Schaal (PNAS, 2002) have shown that the disease severity and fitness are slightly negatively correlated (non-significant) and they further speculate that infection might affect the seed production through different genetic mechanism than the genetic mechanisms that determines disease resistance (outcome of pathogen growth and disease symptoms). Work presented here confirms this as there are very few QTLs that overlapped between fecundity and disease index. Authors have not considered this as a simple explanation and controlling for the disease index in their 

### Minor


